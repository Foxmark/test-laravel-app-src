<?php

namespace Tests\Feature;

use App\Services\ProductInterface;
use Illuminate\Pagination\LengthAwarePaginator;
use Tests\TestCase;

class ProductTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_index()
    {
        $page = new LengthAwarePaginator([], 1, 10, 1);
        $product = $this->mock(ProductInterface::class);
        $product->shouldReceive('getPaginated')->andReturn($page);

        $response = $this->get('/api/product');
        $response->assertStatus(200);
    }

    public function test_show()
    {
        $this->mock(ProductInterface::class, function($mock){
            $mock->shouldReceive('show')->andReturn([]);
        });

        $response = $this->get('/api/product/1');
        $response->assertStatus(200);
    }

    public function test_store()
    {
        $data = [
            'name'        => 'name',
            'description' => 'description',
            'active'      => 1
        ];
        $this->mock(ProductInterface::class, function($mock) {
            $mock->shouldReceive('create')->andReturn([]);
        });

        $response = $this->post('/api/product', $data);
        $response->assertStatus(201);
    }

    public function test_validation_errors()
    {
        $response = $this->post('/api/product', []);
        $response->assertStatus(400);
    }

    public function test_update()
    {
        $data = [
            'name'        => 'name',
            'description' => 'description',
            'active'      => 1
        ];
        $this->mock(ProductInterface::class, function($mock) {
            $mock->shouldReceive('update')->andReturn([]);
        });

        $response = $this->put('/api/product/1', $data);
        $response->assertStatus(200);
    }

    public function test_destroy()
    {
        $this->mock(ProductInterface::class, function($mock) {
            $mock->shouldReceive('delete')->andReturn([]);
        });

        $response = $this->delete('/api/product/1');
        $response->assertStatus(200);
    }
}
