<?php

namespace Database\Factories;

use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name'        => $this->faker->sentence(),
            'description' => $this->faker->paragraph(5),
            'created_at'  => $this->faker->dateTimeBetween('-5 years', 'now'),
            'updated_at'  => $this->faker->dateTimeBetween('-5 years', 'now'),
            'active'      => 1,
        ];
    }

    /**
     * Indicate that the model's active flag is set to 0.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function inactive()
    {
        return $this->state(function (array $attributes) {
            return [
                'active' => 0,
            ];
        });
    }
}
