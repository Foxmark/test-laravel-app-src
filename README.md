# Test App

#### This is a test app showing docker setup for laravel application.

## Design overview
**Laravel 8** is a backbone of the application.
Laravel is very popular php framework with a very good documentation and a large community.
Long term development and maintenance should be achieved relatively easily with large number of developers able to write and develop for Laravel framework.
Everything is powered by fast fpm **PHP 8.0** and controlled by **Nginx**

## Deployment (demo app)
### Overview
This application has been designed for docker.

In [.docker](https://bitbucket.org/Foxmark/test-laravel-app-src/src/master/.docker/) subdirectory of the project you will find all `Dockerfiles` and two `docker-compose` files needed to develop and deploy the app.

- `docker-compose.yml` - designed for production environment.
- `docker-compose-dev.yml` - allows you to develop and test the application.

Running demo app will use three custom containers (see links) but you can use `app-build`, `fpm` and `nginx` folders to build you own images.

The images used in this example are public to keep the presentation simple - in production that would be considered unsafe.

### System requirements:

App will run on any device with `docker` and `docker-compose` installed.
This can be done on a local 'bare metal' or as a clooud solution.
[Amazon ECS](https://aws.amazon.com/ecs/) might be a good option to explore for cloud deployment.
[Docker Swarm](https://docs.docker.com/engine/swarm/) cluster would be a good place to start as it allow to scale the application and it's easy to setup - this can be done locally as well as in the cloud.

### Running the demo app:

**Note: Below instruction assumes you are using linux operating system and bash cmd.**

To see the working app demo the following software should be installed:
- [Docker](https://docs.docker.com/get-docker/).
- [Docker Compose](https://docs.docker.com/compose/install/).

1. You can clone the entire App repository `git clone git@bitbucket.org:Foxmark/test-laravel-app-src.git`
   or just use `docker-compose.yml` and `.env_sample` files from [.docker](https://bitbucket.org/Foxmark/test-laravel-app-src/src/master/.docker/) subdirectory.

2. Navigate to `.docker` directory or your custom folder with `docker-compose.yml` file.
3. Run `cp .env_sample .env` to create `.env` file
4. Open `.env` file and makes sure selected port `HOST_PORT` is not used by any other application (the default value is `8080`)
5. Run `docker-compose up -d`
6. Use your browser to navigate to [http://localhost:8080](http://localhost:8080)
7. Navigate to [http://localhost:8080/test](http://localhost:8080/test) to test the app.
8. Run `docker-compose down -v` to destroy the test app.

## Development

Clone App repository `git clone https://ppasich@bitbucket.org/Foxmark/test_api.git`

You need to run:

`composer install`

`cp .env.example .env`

`php artisan key:generate`

`php artisan migrate:fresh`

Use this [link](https://linuxhint.com/how-to-set-up-file-permissions-for-laravel/) if you have problem with the file permissions

**Note:** `up.sh` and `down.sh` scripts from `.docke` dir allowing you to start and stop development containers.

## Testing

Set of API tests can be found in `tests` [file](https://bitbucket.org/Foxmark/test-laravel-app-src/src/master/tests/)

## Security Vulnerabilities - TODOs

- Implement SSL support for Nginx container: at the moment all traffic between client and server is unencrypted
- Implement User authentication (laravel)
- Implement API key authentication (laravel)


## Links

- [App - Docker Image Repository](https://hub.docker.com/repository/docker/foxmark/test-laravel-app)
- [Fpm - Docker Image Repository](https://hub.docker.com/repository/docker/foxmark/php8.0.0-fpm-buster)
- [Nginx - Docker Image Repository](https://hub.docker.com/repository/docker/foxmark/nginx)
- [Laravel Documentation](https://laravel.com/docs)


Author: Pawel Pasich
