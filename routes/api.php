<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*
| php artisan make:controller --api --resource __NAME__Controller
| php artisan make:model -m __NAME__
| php artisan make:resource __NAME__ --collection
| php artisan make:resource __NAME__Collection
| php artisan make:seeder __NAME__Seeder
| php artisan make:factory __NAME__Factory --model=__NAME__
 */

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


/*
Route::get('/product',[ProductController::class, 'index']);
Route::get('/product/{id}',[ProductController::class, 'show']);
Route::post('/product',[ProductController::class,'store']);
Route::put('/product/{id}',[ProductController::class,'update']);
Route::delete('/product/{id}',[ProductController::class,'destroy']);
*/

Route::apiResource('product', ProductController::class);
