## Building docker image

To avoid error: `COPY failed: Forbidden path outside the build context` when building an image you must:

- cd to root directory of the project
- run `docker build -t test-app -f .docker/app-build/Dockerfile .` 

[Read more](https://stackoverflow.com/questions/27068596/how-to-include-files-outside-of-dockers-build-context)
