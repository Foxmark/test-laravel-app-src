#!/bin/sh
read -p 'Enter App version (XY.Z): ' app_ver
read -p "Deploy app version: ${app_ver} (y/N)?: " ver_ok

if [ "$ver_ok" != "y" ]; then
    echo "Deployment aborted"
    exit 0
fi
echo "Deployment in progress ..."
# Login?
&& docker build -t foxmark/test-laravel-app:$app_ver -f .docker/app-build/Dockerfile . \
&& docker tag foxmark/test-laravel-app:$app_ver foxmark/test-laravel-app:$app_ver \
&& docker push foxmark/test-laravel-app:$app_ver
