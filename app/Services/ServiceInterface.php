<?php

namespace App\Services;

use \Illuminate\Pagination\LengthAwarePaginator;

interface ServiceInterface {
    public function getPaginated(int $items_per_page): LengthAwarePaginator;
    public function show(int $id): array;
    public function update(array $data, int $id) : array;
    public function create(array $data) : array;
    public function delete(int $id);
}
