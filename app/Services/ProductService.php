<?php

namespace App\Services;

use App\Models\Product;
use Illuminate\Pagination\LengthAwarePaginator;

class ProductService implements ProductInterface
{
    private $model;

    public function __construct(Product $model)
    {
        $this->model = $model;
    }

    public function getPaginated(int $items_per_page = 15): LengthAwarePaginator
    {
        return $this->model::paginate($items_per_page);
    }

    public function show(int $id): array
    {
        return $this->getProductModelByID($id)->toArray();
    }

    public function update(array $data, int $id): array
    {
        $product = $this->getProductModelByID($id);
        $product->update($data);
        return $product->toArray();
    }

    public function create(array $data): array
    {
        return $this->model::create($data)->toArray();
    }

    public function delete(int $id)
    {
        return $this->getProductModelByID($id)->delete();
    }

    protected function getProductModelByID(int $id): Product
    {
        return $this->model::findOrFail($id);
    }
}
