<?php

namespace App\Http\Requests;

class ProductRequest extends BaseApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->method() === "POST") {
            return [
                'name'        => 'required|string|max:128',
                'description' => 'required|string|max:4096',
                'active'      => 'required|int|in:0,1'
            ];
        }
        return [
            'name'        => 'nullable|string|max:128',
            'description' => 'nullable|string|max:4096',
            'active'      => 'nullable|int|in:0,1'
        ];
    }
}
