<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $table      = 'products';
    protected $primaryKey = 'id';
    protected $visible    = ['id', 'name', 'description', 'active', 'created_at', 'updated_at'];
    protected $fillable   = ['name', 'description', 'active'];

    protected $casts = [
        'id'     => 'int',
        'active' => 'int'
    ];

    protected function serializeDate(\DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function getCreatedAttribute($value)
    {
        return $value;
    }

    public function getEditedAttribute($value)
    {
        return $value;
    }
}
